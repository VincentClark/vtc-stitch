# VTC Stitch Version 0.4
A simple stitch program that allows you to put together an intro, main video, and outro to test ascetics. 

**Built in Python relies heavily on the python package FFmpeg.**
** Currently only tested on Windows 10 machines waiting for test results from Mac users **

## Requirements
* Python 3.8 or higher
* Pip3
* FFMpeg to be installed

### Setup

./videofiles/Instructions/demo.txt

This file will set the project name, intro, main, dimensions, and outro

Files to be stitched must be places in ./videofiles/Video_Input

File output will be int the ./videofiles/Output folder

The name of the project will be the name of the output file.

#### To Initialize 
ensure that your video files are in the Video_Input folder and the demo.txt file is properly filled out.

stitch() <- excutes the stitching process. 


## Known issues. 
* When using a Jpeg or PNG file as your intro file the exported file's width is not calculated correctly.
* Image files must be the same as your output setting. 
* Audio is currently not working. 
* Only tested with mp4 (h264 and h256) file formats. 
* Must have all three files set. 

## To Do
* Allow for only an intro or outro.
* and sound overley
* Add more control over the output file format
* Web based interface
    * Convert demo.txt to read Json
* Add output log. 
* Move output file to new folder based on project name
## Potential Reactors
* Transition to using subprocess to communicated with FFmpeg instead of the Python library FFMpeg

