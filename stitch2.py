import ffmpeg
import file_reader

th_path = 'videofiles/Instructions/demo.txt'
# --- initial set up ---
vid_path = 'videofiles/Video_Input/'


#class version
drop_location = "videofiles/Output/"

insert_dict = {

}
stub = file_reader.create_stub(th_path)
video_list = [vid_path + stub['intro'], vid_path + stub['main'], vid_path+ stub['outro']]
file_path = "videofiles/Video_Input/"
#should use update or add keys. I don't like this.

insert_dict.update({'width' : stub['width']})
insert_dict.update({'height': stub['height']})
insert_dict.update({'fps': stub['fps']})
insert_dict.update({'project_name': stub['project_name']})
insert_dict.update({'file_list' : video_list})

print(insert_dict['file_list'])



class Stitch():
    # default specs
    # super class will overrite these specs || getters and setters

    _width = '1080'
    _height = '720'
    _use_fps = '25'
    _file_list = []
    _file_build_list = []
    _project_name = "default"
    _output_file = 'default.mp4'


    def __init__(self, dicti=None):
        self.dicti = dicti
        if not self.dicti:
            print("Skipped")
        else:
            print("adding task")
            self._width = dicti["width"]
            self._height = dicti["height"]
            self._use_fps = dicti["fps"]
            self._project_name = dicti['project_name']
            self._output_file = dicti['project_name'] + ".mp4"
            self._file_list = dicti["file_list"]

            print(f"file_list in: {dicti['file_list']}")
            print (f"_file list: {self._file_list}")

    def setWidth(self, width):
        _width = width

    def setHeight(self, height):
        _height = height

    def set_width_height(self, width, height):
        _width = width
        _height = height

    def setFps(self, fps):
        _fps = fps
    def orchastrate(self):
        self.file_ar = self.set_filelist()
        self.process(self.file_ar, self._output_file)


    def set_filelist(self):

        for name in self._file_list:
            file_put = self.build_file(name)
            self._file_build_list.append(file_put)

        return self._file_build_list

    def build_file(self, file_name):
        if file_name[-1] != "g":
            print("Video being used")
            built_file = (ffmpeg.input(file_name).filter(
                'fps', fps=self._use_fps, round='up'
            ).filter('scale', width=self._width, height=self._height))
            print(f'width={self._width} -|- height={self._height}')
        else:
            print("Image being used")
            built_file = ffmpeg.input(file_name, t=3, framerate=self._use_fps, loop=1)
        return built_file

    def process(self, stitch_ar, v_output="default.mp4"):
        #audio = ffmpeg.input().audio.filter("aecho", 0.8, 0.9, 1000, 0.3)
        process = (
            ffmpeg
                .
                concat(stitch_ar[0], stitch_ar[1], stitch_ar[2])
                .output("videofiles/Output/"+v_output)
                .overwrite_output()
                .run()

        )



stitch = Stitch(insert_dict)
stitch.orchastrate()

