# (VTC) - Stich app

## User Stories
The client needs to demo their video files with various intros and exists. In order to keep costs low and not to burden their video production team the user would like to view how the end product would work prior to requesting an edit from their video production team.

The client has promo videos, intro videos, and exit videos, however they are in different frame sizes and frame rates.
 
The client would like an application that would stitch together an intro and outro with their promo file. 

The client would like the transcoding to be done on their local machine. 


## Road Map


### Minnumum Viable Product 
The client would like drop the files into a drop folder, trigger the transcoder, and retrieve the files from an output folder using a specfic file names. 


### Phase 2
The client would like to have control over the dimensions and frame rates of the final product. 
#### Phase 2.4
The client would like to control the name of the output file and input file.  
#### Phase 2.5 
Client would like to have finer control over output settings. 
#### phase 2.5
The client would like to like to use an image instead of an intro
###$ Phase 2.6
The client would like information associated with their end product including dimensions, frame rate, video size. 
The client would like to add an audio overlay and watermark.
### Phase 3
The client would like to process multiple jobs at once with an existing profile. 
### Phase 5
Web based interface to orchestrate the stitching



